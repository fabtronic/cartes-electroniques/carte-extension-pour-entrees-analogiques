# Carte ANALOG INPUT EXTENSION

KiCad-Analog-Extender-x24 = Carte d’extension avec entrées analogiques.

Exemple "[Velleman KA12 Analog Input Extension Shield for Arduino](https://manuals.plus/fr/velleman/analog-input-extension-shield-for-arduino-manual)" ([ASSEMBLY MANUAL](https://www.velleman.eu/downloads/0/assembly/assembly_manual_ka12_en.pdf))

## Caractéristiques

L’Arduino UNO™ est équipé de 6 entrées analogiques et certains projets nécessitent plus d’entrées analogiques. Ce schéma n’utilise que 4 ports I/O (3 numériques / 1 analogique) mais ajoute 24 entrées, soit 29 entrées analogiques au total.

- Utilise les broches 5, 6, 7 et A0 sur platine type Arduino UNO™
- Ajoute 24 entrées analogiques vers l'entrée analogique A0 : 0 - 5 VCC

---

## Recherche de composants

### Multiplexeur analogique 1 canal avec conversion de niveau logique

> CD4051BM - En cours d'approvisionnement
https://fr.rs-online.com/web/p/multiplexeurs-et-demultiplexeurs-ci/6622647

> [CD4051BPWR](https://www.ti.com/product/CD4051B/part-details/CD4051BPWR) - 280 En stock
https://fr.rs-online.com/web/p/multiplexeurs-et-demultiplexeurs-ci/6622533

![Figure 25. 24-to-1 MUX Addressing](Images/CD4051B-Figure_25-24-to-1-MUX-Addressing.png)

![Table 1. Truth Table](Images/CD4051B-Table_1-Truth-Table.png)

### Connecteurs

4x connecteur Mâle, 6x3 contacts sur 3 rangs, pas 2.54mm, Coudé, Traversant

Trois rangées mâles carrées à Angle droit, 3x40 P, 2.54mm, 10 pièces, trois rangées, plaquées or, espace 2.54
https://fr.aliexpress.com/item/32716157137.html

### Réseau de résistances

Réseau de résistance Bourns 100kΩ ±2% Bus, 8 résistances, 1.13W, boîtier SIP série 4600X Traversant
Code commande RS: [522-4087](https://fr.rs-online.com/web/p/reseaux-de-resistances/5224087)
Référence fabricant: 4609X-101-104LF
Marque: Bourns

---

## Liens Internet

- [Arduino UNO R3](https://docs.arduino.cc/hardware/uno-rev3)
- [Path of Light Part II: Multiplexing and Array](https://wp.nyu.edu/jiaxinxu/2019/12/19/pcom-final-iv-path-of-light-part-ii-multiplexing-and-array/)
- [Analog Multiplexer/Demultiplexer - 4051](https://playground.arduino.cc/Learning/4051/)
